package id.web.charzone95.labmobile2017reference;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbarMain)
    Toolbar toolbarMain;

    @BindString(R.string.title_activity_main)
    String titleActivityMain;

    @BindView(R.id.editTextNim)
    EditText editTextNim;

    @BindView(R.id.editTextNama)
    EditText editTextNama;

    @BindView(R.id.editTextAlamat)
    EditText editTextAlamat;

    @BindView(R.id.buttonSave)
    Button buttonSave;

    @BindString(R.string.error_nama)
    String stringErrorNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbarMain);
        setSupportActionBar(toolbarMain);
        setTitle(titleActivityMain);

    }

    @OnClick(R.id.buttonSave)
    public void buttonSaveClick() {
        String nama = editTextNama.getText().toString();
        String nim = editTextNim.getText().toString();
        String alamat = editTextAlamat.getText().toString();

        String textToDisplay = "Halo " + nama  + " (" + nim + ") yang tinggal di " + alamat;
        Toast.makeText(MainActivity.this, textToDisplay, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.buttonNext)
    public void buttonNextClick() {
        String nama = editTextNama.getText().toString();

        if (nama.trim().isEmpty()) {
            editTextNama.setError(stringErrorNama);
            editTextNama.requestFocus();
            return;
        }

        //lanjut
        Intent intent = new Intent(this, LinearLayoutActivity.class);
        intent.putExtra(LinearLayoutActivity.INTENT_KEY_NAMA, nama);
        this.startActivity(intent);
    }
}
