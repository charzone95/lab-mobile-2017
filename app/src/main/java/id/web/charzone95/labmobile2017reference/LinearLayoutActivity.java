package id.web.charzone95.labmobile2017reference;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class LinearLayoutActivity extends AppCompatActivity {
    public static final String INTENT_KEY_NAMA = "nama";


    @BindView(R.id.textViewHalo)
    TextView textViewHalo;

    @BindString(R.string.hello_nama)
    String stringHelloNama;

    @BindViews({
            R.id.imageViewGokuNormal, R.id.imageViewGoku1, R.id.imageViewGoku2,
            R.id.imageViewGoku3, R.id.imageViewGokuGod, R.id.imageViewGokuBlue,
            R.id.imageViewGokuUltraInstinct})
    List<ImageView> listGoku;

    String nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layout);
        ButterKnife.bind(this);


        //ambil data dari intent
        Intent intent = this.getIntent();
        nama = intent.getStringExtra(INTENT_KEY_NAMA);

        String textToDisplay = stringHelloNama.replace("{nama}", nama);
        textViewHalo.setText(textToDisplay);


        List<String> listUrl = new ArrayList<>();
        listUrl.add("http://charzone95.web.id/mobile/goku_normal.jpg");
        listUrl.add("http://charzone95.web.id/mobile/goku_ss1.jpg");
        listUrl.add("http://charzone95.web.id/mobile/goku_ss2.jpg");
        listUrl.add("http://charzone95.web.id/mobile/goku_ss3.jpg");
        listUrl.add("http://charzone95.web.id/mobile/goku_ssblue.jpg");
        listUrl.add("http://charzone95.web.id/mobile/goku_ssgod.jpg");
        listUrl.add("http://charzone95.web.id/mobile/goku_ultra_instinct.jpg");

        int index = 0;
        for (ImageView imageView : listGoku) {
            GlideApp.with(this)
                    .load(listUrl.get(index))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.dragon_ball)
                    .fitCenter()
                    .skipMemoryCache(true)
                    .into(imageView);

            index++;
        }

    }
}
